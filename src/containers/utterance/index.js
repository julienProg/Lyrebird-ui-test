import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateStatusUtterance, cancelStatusId } from '../../modules/utterance';

import Chip from '@material-ui/core/Chip';
import Button from '@material-ui/core/Button';
import PlayCircleOutline from '@material-ui/icons/PlayCircleOutline';
import FileDownload from '@material-ui/icons/FileDownload';

import { downloadFile, createGetHttp } from '../../utils/request';

class Utterance extends Component {
  playMusic = () => {
    createGetHttp('/utterances/' + this.props.utteranceId + '/download').then(
      res => {
        var myAudio = new Audio(res.request.responseURL);
        myAudio.load();
        myAudio.play();
      }
    );
  };

  download = () => {
    downloadFile('/utterances/' + this.props.utteranceId + '/download');
  };

  componentDidMount() {
    this.props.updateStatusUtterance(
      this.props.async_job_id,
      this.props.utteranceId
    );
  }

  componentWillUnmount() {
    this.props.cancelStatusId(this.props.async_job_id);
  }

  renderText() {
    if (this.props.text.length < 50) {
      return this.props.text;
    } else {
      let text = this.props.text.substring(0, 50) + '...';
      return <div>{text}</div>;
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col1">{this.renderText()}</div>
        <div className="col2">
          {Object.keys(this.props.metadata).map(key => (
            <Chip key={key} label={key + ' : ' + this.props.metadata[key]} />
          ))}
        </div>
        <div className="col3">{this.props.status}</div>
        <div className="col4">
          <Button
            disabled={this.props.status !== 'done'}
            variant="fab"
            mini
            color="secondary"
            onClick={() => this.playMusic()}
          >
            <PlayCircleOutline />
          </Button>
        </div>
        <div className="col5">
          <Button
            disabled={this.props.status !== 'done'}
            variant="fab"
            mini
            color="secondary"
            onClick={() => this.download()}
          >
            <FileDownload />
          </Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  utterance: state.utterance.utterance
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateStatusUtterance,
      cancelStatusId
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Utterance);
