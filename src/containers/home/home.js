// @flow
import React, { Component } from 'react';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getVoice } from '../../modules/voice';
import { addUtterance } from '../../modules/utterance';

import FormDialogUtterance from '../../components/formDialogUtterance';
import RowVoice from '../../components/rowVoice';
import ListUtterance from '../listUtterance';

import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import './home.css';

const styles = theme => ({
  header: {
    color: theme.palette.primary.main
  }
});

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.getVoice();
  }

  state = {
    keyCLicked: '',
    openModal: false,
    openUtterance: false,
    titleVoiceModal: '',
    metadata: {}
  };

  onClickRow = key => {
    this.setState(prevState => {
      return {
        keyCLicked: prevState.keyCLicked !== key ? key : ''
      };
    });
  };

  onClickAddUtterance = (e, nameVoice, metadata) => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    this.setState(() => {
      return {
        openModal: true,
        nameVoiceModal: nameVoice,
        metadata: metadata
      };
    });
  };

  onCloseModal = () => {
    this.setState(() => {
      return {
        openModal: false,
        nameVoiceModal: '',
        metadata: {}
      };
    });
  };

  onClickUtterance = (e, nameVoice) => {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    this.setState(() => {
      return {
        openUtterance: true,
        nameVoiceModal: nameVoice
      };
    });
  };

  onCloseUtterance = () => {
    this.setState(() => {
      return {
        openUtterance: false,
        nameVoiceModal: ''
      };
    });
  };

  render() {
    return (
      <div className="home">
        {!this.state.openUtterance && (
          <div>
            <Typography variant="title">List of voices</Typography>
            <Card className="card-table">
              <div className="table">
                <div className={'header ' + this.props.classes.header}>
                  <div className="col1">Title</div>
                  <div className="col2">Description</div>
                  <div className="col3">Number of utterances</div>
                </div>
                {Object.keys(this.props.voice).map(key => (
                  <RowVoice
                    key={key}
                    onClick={() => this.onClickRow(key)}
                    className={
                      'row ' + (this.state.keyCLicked === key ? 'active' : '')
                    }
                    name={this.props.voice[key].name}
                    description={this.props.voice[key].description}
                    count={this.props.voice[key].count}
                    onListUtterance={e =>
                      this.onClickUtterance(e, this.props.voice[key].name)
                    }
                    isDetail={this.state.keyCLicked === key}
                    onAddUtterance={e =>
                      this.onClickAddUtterance(
                        e,
                        this.props.voice[key].name,
                        this.props.voice[key].supported_metadata
                      )
                    }
                  />
                ))}
              </div>
            </Card>
          </div>
        )}
        {this.state.openModal && (
          <FormDialogUtterance
            open={this.state.openModal}
            nameVoice={this.state.nameVoiceModal}
            metadata={this.state.metadata}
            onCancel={this.onCloseModal}
            onCreate={data =>
              this.props.addUtterance(this.state.keyCLicked, data)
            }
          />
        )}
        {this.state.openUtterance && (
          <ListUtterance
            nameVoice={this.state.nameVoiceModal}
            voiceId={this.state.keyCLicked}
            onReturn={() => this.onCloseUtterance()}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.voice.isLoading,
  voice: state.voice.voice,
  hasError: state.voice.hasError
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      changePage: key => push('/utterance/' + key),
      getVoice,
      addUtterance
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Home));
