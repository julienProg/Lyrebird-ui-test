import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { fetchedUtterance, cancelStatus } from '../../modules/utterance';
import classNames from 'classnames';

import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import NavigateBefore from '@material-ui/icons/NavigateBefore';

import Utterance from '../utterance';

import './listUtterance.css';

const styles = theme => ({
  header: {
    color: theme.palette.primary.main
  },
  leftIcon: {
    marginRight: theme.spacing.unit
  },
  button: {
    'margin-bottom': '30px'
  },
  iconSmall: {
    fontSize: 20
  },
  marginRight: {
    'margin-right': '10px'
  }
});

class ListUtterance extends Component {
  state = {
    isFetched: false,
    search: ''
  };

  componentDidMount() {
    this.props.fetchedUtterance(this.props.voiceId, () => {
      this.setState(() => {
        return {
          isFetched: true
        };
      });
    });
  }

  componentWillUnmount() {
    this.props.cancelStatus();
  }

  refineBySearch = (search = '', listUtterance) => {
    let result = {};
    Object.keys(listUtterance).forEach(e => {
      if (listUtterance[e].text.indexOf(search) !== -1) {
        result[e] = listUtterance[e];
      }
    });
    return result;
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  render() {
    return (
      <div
        className={'list-utterance ' + (this.props.voiceId ? '' : 'margin-tab')}
      >
        {this.props.onReturn && (
          <Button
            onClick={this.props.onReturn}
            size="small"
            variant="contained"
            color="primary"
            className={this.props.classes.button}
          >
            <NavigateBefore
              className={classNames(
                this.props.classes.leftIcon,
                this.props.classes.iconSmall
              )}
            />
            <span className={this.props.classes.marginRight}>Back</span>
          </Button>
        )}
        <Typography variant="title">List of utterances</Typography>
        <div className="discreet">
          {this.props.nameVoice ? '(voice : ' + this.props.nameVoice + ')' : ''}
        </div>
        <TextField
          label="Search"
          id="search"
          fullWidth
          className="search"
          onChange={this.handleChange('search')}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            )
          }}
        />
        <Card className="card-table">
          <div className="table">
            <div className={'header ' + this.props.classes.header}>
              <div className="col1">Text</div>
              <div className="col2">Metadata</div>
              <div className="col3">Status</div>
              <div className="col4">Play</div>
              <div className="col5">Download</div>
            </div>
            {this.state.isFetched &&
              Object.keys(
                this.refineBySearch(this.state.search, this.props.utterance)
              ).map(key => (
                <Utterance
                  key={key}
                  async_job_id={this.props.utterance[key].async_job_id}
                  text={this.props.utterance[key].text}
                  metadata={this.props.utterance[key].metadata}
                  status={this.props.utterance[key].status}
                  utteranceId={key}
                />
              ))}
          </div>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  utterance: state.utterance.utterance
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchedUtterance,
      cancelStatus
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ListUtterance));
