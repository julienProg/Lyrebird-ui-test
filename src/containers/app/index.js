import React from 'react';
import { Route, Link } from 'react-router-dom';
import Home from '../home/home';
import ListUtterance from '../listUtterance';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  root: {
    display: 'flex',
    'flex-direction': 'column',
    height: '100vh'
  },
  content: {
    backgroundColor: theme.palette.background.default,
    flex: 1
  },
  flex: {
    flex: 1
  },
  link: {
    'text-decoration': 'none',
    color: 'inherit'
  }
});

const App = props => {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <header>
        <AppBar position="static">
          <Toolbar>
            <Typography
              className={classes.flex}
              variant="title"
              color="inherit"
            >
              App name
            </Typography>
            <Link className={classes.link} to="/">
              <Button color="inherit">My voices</Button>
            </Link>
            <Link className={classes.link} to="/utterance">
              <Button color="inherit">My utterances</Button>
            </Link>
          </Toolbar>
        </AppBar>
      </header>

      <main className={classes.content}>
        <Route exact path="/" component={Home} />
        <Route exact path="/utterance" component={ListUtterance} />
      </main>
    </div>
  );
};

export default withStyles(styles)(App);
