import { createMuiTheme } from '@material-ui/core/styles';

export const THEME = createMuiTheme({
  palette: {
    primary: {
      light: '#9FA8DA',
      main: '#3F51B5',
      dark: '#283593',
      contrastText: '#fff'
    },
    secondary: {
      light: '#A7FFEB',
      main: '#1DE9B6',
      dark: '#00BFA5',
      contrastText: '#000'
    }
  }
});
