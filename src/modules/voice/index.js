import {
  VOICE_FETCHED,
  VOICE_REQUESTED,
  VOICE_ERROR_FETCHED,
  VOICE_ADD_UTTERANCE
} from './actions';

import { createGetHttp } from '../../utils/request';

const initialState = {
  isLoading: true,
  voice: {},
  hasError: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case VOICE_REQUESTED:
      return {
        ...state,
        isLoading: true
      };

    case VOICE_ERROR_FETCHED:
      return {
        ...state,
        hasError: true,
        isLoading: false
      };

    case VOICE_FETCHED:
      return {
        ...state,
        isLoading: false,
        voice: action.voice
      };

    case VOICE_ADD_UTTERANCE:
      let voice = JSON.parse(JSON.stringify(state.voice));
      voice[action.voiceId].count++;
      return {
        ...state,
        isLoading: false,
        voice: voice
      };

    default:
      return state;
  }
};

export const getVoice = () => {
  return dispatch => {
    dispatch({
      type: VOICE_REQUESTED
    });
    createGetHttp('/voices')
      .then(res => {
        let voice = {};
        res.data.results.forEach(e => {
          voice[e.id] = e;
        });
        dispatch({
          type: VOICE_FETCHED,
          voice: voice
        });
      })
      .catch(err => {
        dispatch({
          type: VOICE_ERROR_FETCHED
        });
      });
  };
};
