import {
  UTTERANCE_FETCHED,
  // ADD_UTTERANCE,
  RESET_UTTERANCE,
  UPDATE_SATE_UTTERANCE
} from './actions';
import { createGetHttp, createPostHttp } from '../../utils/request';
import { VOICE_ADD_UTTERANCE } from '../voice/actions';

const initialState = {
  utterance: {},
  isLoading: false
};

let listTimeOut = [];
let objSuscribe = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case UTTERANCE_FETCHED:
      return {
        ...state,
        utterance: action.utterance
      };

    // case ADD_UTTERANCE:
    //   return state;

    case RESET_UTTERANCE:
      return {
        ...state,
        utterance: {}
      };

    case UPDATE_SATE_UTTERANCE:
      let copyUtterance = JSON.parse(JSON.stringify(state.utterance));
      copyUtterance[action.utteranceId].status = action.status;
      return {
        ...state,
        utterance: copyUtterance
      };

    default:
      return state;
  }
};

export const fetchedUtterance = (voiceId, callback) => {
  return dispatch => {
    dispatch({
      type: RESET_UTTERANCE
    });

    let query = {};
    if (voiceId) {
      query['voice_id'] = voiceId;
    }
    createGetHttp('/utterances', query).then(res => {
      let utterance = {};
      res.data.results.forEach(e => {
        utterance[e.id] = e;
      });
      dispatch({
        type: UTTERANCE_FETCHED,
        utterance
      });
      callback();
    });
  };
};

export const addUtterance = (voiceId, json) => {
  return dispatch => {
    createPostHttp('/voices/' + voiceId + '/generate_async', [json]).then(
      res => {
        dispatch({
          type: VOICE_ADD_UTTERANCE,
          voiceId
        });

        // createGetHttp('/utterances', {async_job_id:res.data.async_job_id}).then(res => {
        //   console.log(res)
        // });
      }
    );
  };
};

export const updateStatusUtterance = (asyncId, utteranceId) => {
  return dispatch => {
    objSuscribe[utteranceId] = true;
    updateStatus(dispatch, asyncId, utteranceId);
  };
};

export const cancelStatus = () => {
  return dispatch => {
    listTimeOut.forEach(e => {
      clearTimeout(e);
    });
    listTimeOut = [];
    objSuscribe = {};
    dispatch({
      type: RESET_UTTERANCE
    });
  };
};

export const cancelStatusId = utteranceId => {
  return dispatch => {
    objSuscribe[utteranceId] = false;
  };
};

let updateStatus = (dispatch, asyncId, utteranceId, pos = -1) => {
  if (!objSuscribe[utteranceId]) return;
  createGetHttp('/async_jobs/' + asyncId).then(res => {
    if (res.data.status !== 'done') {
      let posF = pos;
      if (pos === -1) {
        pos = listTimeOut.length;
      }
      listTimeOut[posF] = setTimeout(() => {
        this.updateState(dispatch, asyncId, utteranceId, posF);
      }, 5000);
    }
    dispatch({
      type: UPDATE_SATE_UTTERANCE,
      utteranceId: utteranceId,
      status: res.data.status
    });
  });
};
