import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import voice from './voice';
import utterance from './utterance';

export default combineReducers({
  routing: routerReducer,
  voice,
  utterance
});
