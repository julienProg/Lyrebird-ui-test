import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';

import './formDialogUtterance.css';

export default class FormDialogUtterance extends Component {
  state = {
    text: ''
  };

  handleOnCreate = () => {
    let json = {
      text: this.state.text,
      metadata: {}
    };
    Object.keys(this.state).forEach(key => {
      if (key !== 'text') {
        json.metadata[key] = this.state[key];
      }
    });
    this.props.onCreate(json);
    this.props.onCancel();
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  capitalizeText = text => {
    return text.charAt(0).toUpperCase() + text.slice(1);
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.props.onCancel}
        aria-labelledby="form-dialog-title"
        fullWidth
      >
        <DialogTitle id="form-dialog-title">Create a utterance</DialogTitle>
        <DialogContent>
          <TextField
            id="name-voice"
            label="Name voice"
            multiline
            rowsMax="4"
            value={this.props.nameVoice}
            margin="normal"
            disabled
            fullWidth
          />
          <TextField
            required
            id="text-audio"
            label="Text"
            multiline
            rowsMax="4"
            value={this.state.text}
            onChange={this.handleChange('text')}
            margin="normal"
            placeholder="Put your text"
            fullWidth
          />
          {this.props.metadata &&
            Object.keys(this.props.metadata).map(key => (
              <TextField
                key={'select' + key}
                id={'select' + key}
                select
                label={this.capitalizeText(key)}
                value={this.state[key] || this.props.metadata[key].default}
                onChange={this.handleChange(key)}
                margin="normal"
                fullWidth
              >
                {this.props.metadata[key].values.map(option => (
                  <MenuItem key={'select' + key + option} value={option}>
                    {this.capitalizeText(option)}
                  </MenuItem>
                ))}
              </TextField>
            ))}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.onCancel} color="primary">
            Cancel
          </Button>
          <Button
            disabled={this.state.text.length === 0}
            onClick={() => this.handleOnCreate()}
            color="primary"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}
