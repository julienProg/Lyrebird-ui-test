import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';

export default class RowVoice extends Component {
  render() {
    return (
      <div onClick={this.props.onClick} className={this.props.className}>
        <div className="col1">{this.props.name}</div>
        <div className="col2">{this.props.description}</div>
        <div className="col3">
          <Chip label={this.props.count} />
        </div>
        {this.props.isDetail && (
          <div className="col4">
            <Button color="primary" onClick={this.props.onAddUtterance}>
              Add a Utterance
            </Button>
            <Button color="primary" onClick={this.props.onListUtterance}>
              See the utterances
            </Button>
          </div>
        )}
      </div>
    );
  }
}
