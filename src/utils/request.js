import axios from 'axios';

var instance = axios.create({
  baseURL: process.env.REACT_APP_URL,
  timeout: 5000,
  headers: { Authorization: 'Bearer ' + process.env.REACT_APP_API_KEY }
});

export function createGetHttp(path, urlParams = {}) {
  return instance.get(path, { params: urlParams });
}

export function createPostHttp(path, data = {}) {
  return instance.post(path, data);
}

export function downloadFile(path) {
  instance.get(path).then(response => {
    window.location = response.request.responseURL;
  });
}
